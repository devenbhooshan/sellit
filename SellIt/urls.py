from django.conf.urls import patterns, include, url
from django.contrib.auth import *
from django.contrib import admin
import os.path
from django.views.generic import TemplateView
site_media = os.path.join(
os.path.dirname(__file__), 'templates/static'
)

admin.autodiscover()
from app.views import  *
urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'SellIt.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    
    url(r'^$', home),
    url(r'^search/$', search),
    url(r'^cat/(?P<cat_id>\d+)/$', category_page),
    url(r'^register/success/$',  TemplateView.as_view(template_name ='registration/success.html')),
    url(r'^logout/$', logout_page),    
    url(r'^order/(?P<item_id>\d+)/$', order),
    url(r'^item_feedback/$', item_feedback),        
    url(r'^order/submit/$', order_submit),        
    url(r'^track_order/$', track_order),
    (r'^register/$', register_page),

    url(r'^login/$', 'django.contrib.auth.views.login'),    
    url(r'^admin/', include(admin.site.urls)),
    url(r'^detail/(?P<item_id>\d+)/$', detail),
    url(r'^site_media/(?P<path>.*)$', 'django.views.static.serve',
{ 'document_root': site_media }),

)
