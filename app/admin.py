from django.contrib import admin
from app.models import *
from django.contrib.auth.models import User
# Register your models here.
class OrderAdmin(admin.ModelAdmin):
	list_filter=('order_by','order_status','ordered_on')
class ItemAdmin(admin.ModelAdmin):
	list_filter=('item_for','isInStock')
admin.site.register(Order,OrderAdmin)	
admin.site.register(Item,ItemAdmin)
admin.site.register(ItemType)
admin.site.register(ItemToItemType)
#admin.site.register(CloselyRelated)
admin.site.register(ItemPicture)
#admin.site.register(Order)
class OrderedItemAdmin(admin.ModelAdmin):
	list_filter=('order__ordered_on','item__isInStock','item',)

admin.site.register(OrderedItem,OrderedItemAdmin)
#admin.site.register(UserItemViewLog)
#admin.site.register(ReviewsForItems)