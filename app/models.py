from django.db import models
from django.contrib.auth.models import User


class Item(models.Model):
	name=models.CharField(max_length=20,default='No')
	description=models.TextField()
	selling_price=models.FloatField()
	cost_price=models.FloatField()
	added_by= models.ForeignKey(User)
	SEX = (
        (u'M', u'Male'),
        (u'F', u'Female'),
        (u'B', u'Both'),
        )
	SIZE = (
        (u'S', u'Small'),
        (u'M', u'Medium'),
        (u'L', u'Large'),
        (u'XL', u'Extra Large'),
        )	
	item_for=models.CharField(max_length=2, null=True, choices=SEX, default=SEX[0][0])
	size_for=models.CharField(max_length=2, null=True, choices=SIZE, default=SIZE[0][0])
	isInStock=models.BooleanField(default=True)
	stock_left=models.IntegerField()
	def __str__(self):              
    						return self.description

class CloselyRelated(models.Model):
	related_from= models.ForeignKey(Item)
	related_to= models.ForeignKey(Item,related_name="tags",related_query_name="tag")

class ItemType(models.Model):
	type_name=models.CharField(max_length=200)
	def __str__(self):              
    						return self.type_name

class ItemToItemType(models.Model):
	item = models.ForeignKey(Item)
	itemtype = models.ForeignKey(ItemType)	
	class Meta:
		verbose_name="Item Relation";
		verbose_name_plural="Item Relations";
	def __str__(self):              
    						return self.itemtype.type_name+":"+self.item.description

class ItemPicture(models.Model):
	url = models.URLField(unique=True)
	item = models.ForeignKey(Item)
	thumbnail=models.URLField(unique=True)
	def __str__(self):              
    						return "pic_for:"+self.item.description

class Order(models.Model):
	order_by=models.ForeignKey(User)
	STATUS = (
		
        (u'E', u'Ordered'),
        (u'S', u'Sent'),
        (u'F', u'Finished'),
        )
	order_status=models.CharField(max_length=2, null=True, choices=STATUS, default=STATUS[0][0])
	shipping_address=models.TextField()
	quantity=models.IntegerField(default=0)
	ordered_on=models.DateTimeField(auto_now=True,auto_now_add=True)
	total_price=models.FloatField()
	delivery_charges=models.IntegerField(default=0)	
	def _get_total(self) :
		return self.total_price + self.delivery_charges
	total = property(_get_total)
	def __str__(self):              
    						return "Order Id :"+str(self.id)

class OrderedItem(models.Model):
	item = models.ForeignKey(Item)
	order = models.ForeignKey(Order)
	def __str__(self):              
    					return str(self.order)+" :"+self.item.description	

class UserWishList(models.Model):
	user= models.ForeignKey(User)
	item = models.ForeignKey(Item)
	wished_on=models.DateTimeField(auto_now=False,auto_now_add=False)
	class Meta:
	    unique_together = ('item', 'user')

class UserItemViewLog(models.Model):
	item = models.ForeignKey(Item)
	saw_on=models.DateTimeField(auto_now=True,auto_now_add=True)	
	user= models.ForeignKey(User)
	class Meta:
	    unique_together = ('item', 'user')
	def __str__(self):              
    					return str(self.user)+" :"+str(self.item.id)	    


class ReviewsForItems(models.Model):
	user= models.ForeignKey(User)
	item = models.ForeignKey(Item)
	STAR=(
		(u'1', u'Very Poor'),
        (u'2', u'Poor'),
        (u'3', u'Average'),
        (u'4', u'Good'),
        (u'5', u'Very Good'),
		)	
	stars_given=models.CharField(max_length=2, null=True, choices=STAR, default=STAR[0][0])
	comment =models.TextField()

class UserSearchLog():
	item = models.ForeignKey(Item)
	searched_on=models.DateTimeField(auto_now=True,auto_now_add=True)	
	user= models.ForeignKey(User)
