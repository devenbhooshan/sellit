import  math ;
from django.db.models import Avg
from django.contrib.auth import authenticate, login
from django.core import serializers
try: import simplejson as json
except ImportError: import json
from django.shortcuts import render
from django.http import HttpResponse, Http404
from django.contrib.auth.models import User
from django.template.loader import get_template
from django.template import Context
from django.shortcuts import render_to_response
from django.shortcuts import render,get_object_or_404
from django.http import HttpResponseRedirect
from django.contrib.auth import logout
from app.models import *
from app.forms import *
from django.template import RequestContext
import datetime


def home(request):
	category=ItemType.objects.all()
	i=Item.objects.all();
	item=ItemPicture.objects.filter(item=i);
	
	if request.user.is_authenticated():
		recently_viewd_id=UserItemViewLog.objects.filter(user=request.user).order_by('-saw_on').values('item')
		recently_viewd=ItemPicture.objects.filter(item=recently_viewd_id)
		return render_to_response(
		'index.html',
		{ 'user': request.user ,'all_items' :item,'category':category,'cat_id':0,'recently_viewd':recently_viewd}
		)
	else :
		return render_to_response(
		'index.html',
		{ 'user': request.user ,'all_items' :item,'category':category,'cat_id':0,	}
		)
		

def order_submit(request):
	if request.method == 'POST':
		total_cost=request.POST.get('id_totalprice','')
		address=request.POST.get('id_address','')
		itemId=request.POST.get('id_itemid','')
		order=Order(shipping_address=address,total_price=total_cost,order_by=request.user)
		order.save()
		item=get_object_or_404(Item,pk=itemId)
		orderedItem=OrderedItem(item=item,order=order)
		orderedItem.save();
		variables = RequestContext(request, { 
			'address':address,
			'cost':total_cost,
			'itemId':itemId,
			'orderid':order.id
			})
		return render_to_response('order_success.html',variables)

def track_order(request):
	if request.user.is_authenticated():
		ordered=Order.objects.filter(order_by=request.user, order_status='E')
		finished=Order.objects.filter(order_by=request.user, order_status='F')
		sent=Order.objects.filter(order_by=request.user, order_status='S')

		ordered_item=OrderedItem.objects.filter(order=ordered)
		finished_item=OrderedItem.objects.filter(order=finished)
		sent_item=OrderedItem.objects.filter(order=sent)
		context={'user':request.user,'ordered':ordered_item,'finished':finished_item,'sent':sent_item}
		return render(request,'track_order.html',context)
	return HttpResponseRedirect('/login/')

def logout_page(request):
	logout(request)
	return HttpResponseRedirect('/')
def detail(request, item_id):
	itembought=OrderedItem.objects.filter(item=item_id)

	average=ReviewsForItems.objects.filter(item=item_id).aggregate(Avg('stars_given'))
	
	if average['stars_given__avg'] != None :
		avg=int((average['stars_given__avg']))
	else :
		avg=0

	if request.user.is_authenticated():
		feedback=ReviewsForItems.objects.filter(item=item_id).exclude(user =request.user)
	else :
		feedback=ReviewsForItems.objects.filter(item=item_id)
	related_item_type=ItemToItemType.objects.filter(item=item_id).values("itemtype")
	related_item_id=ItemToItemType.objects.filter(itemtype=related_item_type).values("item")
	related_item=ItemPicture.objects.filter(item=related_item_id)
	item_detail=get_object_or_404(Item,pk=item_id)
	now = datetime.datetime.now()
	cart=[];
	userFeedbackYN={'given':False}
	if request.user.is_authenticated():
		cart=Order.objects.filter(order_status='C',order_by=request.user);
		RecentlyViewed=UserItemViewLog(item=item_detail,user=request.user,saw_on=now)
		
		userFeedback=ReviewsForItems.objects.filter(item=item_id,user=request.user)

		if userFeedback.count() == 1:
			userFeedbackYN={'given':True,'feedback':userFeedback}
		try : 
			RecentlyViewed.save()
		except :
			log=UserItemViewLog.objects.filter(item=item_detail,user=request.user)
			log.delete()
			RecentlyViewed.save()
			print "Hello"
	else :	
		print "User Not Authenticated: views.py Line No : 70 ";
	item_pictures=get_object_or_404(ItemPicture,item=item_id)
	context={'item_detail':item_detail,
			'item_pictures':item_pictures,
			'cart':cart,'related_item':related_item,
			'feedback':feedback,'userFeedbackYN':userFeedbackYN,
			'avg_rating':avg,
			'itembought':itembought,
			}		
	return render(request,'detail.html',context)

def order(request,item_id):
	if request.user.is_authenticated():		
		item=get_object_or_404(Item,pk=item_id)
		context ={'user': request.user,'item_detail':item }
		return render(request,'order.html',context)
	else :
		return HttpResponseRedirect('/login/')	
	
	


def search(request):
	data = serializers.serialize("json", Item.objects.all())

	jsondata = json.dumps({'item':data})
	return HttpResponse(jsondata, mimetype='application/json')

def category_page(request,cat_id):

	category=ItemType.objects.all()
	item_id=ItemToItemType.objects.filter(itemtype=cat_id)
	i=[]
	for item in item_id :
		i.append(get_object_or_404(Item,id=item.item.id))
	item=[]
	for itt in i :
		item.append(get_object_or_404(ItemPicture,item=itt))
	
	if request.user.is_authenticated():
		recently_viewd_id=UserItemViewLog.objects.filter(user=request.user).order_by('-saw_on').values('item')
		recently_viewd=ItemPicture.objects.filter(item=recently_viewd_id)
		return render_to_response(
		'index.html',
		{ 'user': request.user ,'all_items' :item,'category':category,'cat_id':cat_id,'recently_viewd':recently_viewd}
		)
	else :
		return render_to_response(
		'index.html',
		{ 'user': request.user ,'all_items' :item,'category':category,'cat_id':cat_id,	}
		)
	

def register_page(request):
	if request.method == 'POST':
		form = RegistrationForm(request.POST)
		if form.is_valid():
			user = User.objects.create_user(
			username=form.cleaned_data['username'],
			password=form.cleaned_data['password1'],
			email=form.cleaned_data['email'],
			#address=form.cleaned_data['address']
		)
			return HttpResponseRedirect('/register/success/')
	else:
		form = RegistrationForm()
	variables = RequestContext(request, {
	'form': form
	})
	return render_to_response(
		'registration/register.html',
		variables
		)
def item_feedback(request):
	jsondata = json.dumps({'success':False})
	if request.method == 'POST':
		star=request.POST.get('star','')
		feedback=request.POST.get('feedback','')
		user=request.user
		item_id=request.POST.get('item_id','')
		item=get_object_or_404(Item,pk=item_id)
		feed=ReviewsForItems(user=user,item=item,comment=feedback,stars_given=int(star))
		feed.save()
		jsondata = json.dumps({'success':True})
	return HttpResponse(jsondata, mimetype='application/json')

